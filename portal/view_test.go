package portal

import (
	"os"
	"testing"
	"text/tabwriter"

	"github.com/stretchr/testify/assert"

	"google.golang.org/protobuf/encoding/protojson"

	"gitlab.com/mergetb/api/portal/v1/go"
)

const tree = `{
	"HighestStatus": "Success",
	"LastUpdated": "2024-06-21T03:23:03.451612003Z",
	"NumChildTasks": "39",
	"Task": {
		"Id": "root",
		"Name": "test.hello.ctran",
		"Message": {
			"Level": "Info",
			"Message": "Materialization Management: test.hello.ctran"
		},
		"StatusValue": "Success",
		"FirstUpdated": "2024-06-21T03:22:48.282349594Z",
		"LastUpdated": "2024-06-21T03:23:03.451612003Z"
	},
	"Subtasks": [
		{
			"HighestStatus": "Success",
			"LastUpdated": "2024-06-21T03:22:57.766821200Z",
			"NumChildTasks": "2",
			"Task": {
				"Id": "/materializations/ctran/hello/test",
				"Name": "Portal Operations",
				"Message": {
					"Level": "Info",
					"Message": "All portal operations for test.hello.ctran"
				},
				"StatusValue": "Success",
				"FirstUpdated": "2024-06-21T03:22:48.282349594Z",
				"LastUpdated": "2024-06-21T03:22:57.766821200Z"
			},
			"Subtasks": [
				{
					"HighestStatus": "Success",
					"LastUpdated": "2024-06-21T03:22:57.766821200Z",
					"NumChildTasks": "2",
					"Task": {
						"Id": "/materializations/ctran/hello/test",
						"Name": "Materialization: test.hello.ctran",
						"Message": {
							"Level": "Info",
							"Message": "Manage materialization: test.hello.ctran"
						},
						"StatusValue": "Success",
						"FirstUpdated": "2024-06-21T03:22:48.282349594Z",
						"LastUpdated": "2024-06-21T03:22:57.766821200Z"
					},
					"Subtasks": [
						{
							"HighestStatus": "Success",
							"LastUpdated": "2024-06-21T03:22:57.366931678Z",
							"NumChildTasks": "0",
							"Task": {
								"Id": "/mtz/mtzs",
								"Name": "/mtz/mtzs",
								"Message": {
									"Level": "Info",
									"Message": "Manage materializations"
								},
								"StatusValue": "Success",
								"FirstUpdated": "2024-06-21T03:22:48.282349594Z",
								"LastUpdated": "2024-06-21T03:22:57.366931678Z"
							},
							"Subtasks": [
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:22:57.366931678Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/materializations/ctran/hello/test",
										"Name": "/materializations/ctran/hello/test",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:22:48.282349594Z",
										"LastUpdated": "2024-06-21T03:22:57.366931678Z"
									},
									"Subtasks": []
								}
							]
						},
						{
							"HighestStatus": "Success",
							"LastUpdated": "2024-06-21T03:22:57.766821200Z",
							"NumChildTasks": "0",
							"Task": {
								"Id": "/wgsvc/WgEnclave",
								"Name": "/wgsvc/WgEnclave",
								"Message": {
									"Level": "Info",
									"Message": "Manage wg enclave data"
								},
								"StatusValue": "Success",
								"FirstUpdated": "2024-06-21T03:22:48.282480853Z",
								"LastUpdated": "2024-06-21T03:22:57.766821200Z"
							},
							"Subtasks": [
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:22:57.766821200Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/materializations/ctran/hello/test",
										"Name": "/materializations/ctran/hello/test",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:22:48.282480853Z",
										"LastUpdated": "2024-06-21T03:22:57.766821200Z"
									},
									"Subtasks": []
								}
							]
						}
					]
				}
			]
		},
		{
			"HighestStatus": "Success",
			"LastUpdated": "2024-06-21T03:23:03.451612003Z",
			"NumChildTasks": "35",
			"Task": {
				"Id": "test.hello.ctran",
				"Name": "moddeter: test.hello.ctran",
				"Message": {
					"Level": "Info",
					"Message": "Create materialization test.hello.ctran"
				},
				"StatusValue": "Success",
				"FirstUpdated": "2024-06-21T03:22:57.157955986Z",
				"LastUpdated": "2024-06-21T03:23:03.451612003Z"
			},
			"Subtasks": [
				{
					"HighestStatus": "Success",
					"LastUpdated": "2024-05-18T05:33:19.573417096Z",
					"NumChildTasks": "2",
					"Task": {
						"Id": "test.hello.ctran/metalCreateOps",
						"Name": "metalCreateOps",
						"Message": {
							"Level": "Info",
							"Message": "Image the baremetal resources"
						},
						"StatusValue": "Success",
						"FirstUpdated": "2024-06-21T03:22:57.151985858Z",
						"LastUpdated": "2024-05-18T05:33:19.573417096Z"
					},
					"Subtasks": [
						{
							"HighestStatus": "Success",
							"LastUpdated": "2024-05-18T05:33:13.610186820Z",
							"NumChildTasks": "0",
							"Task": {
								"Id": "/metal/power.ifr0",
								"Name": "/metal/power.ifr0",
								"Message": {
									"Level": "Info",
									"Message": "Metal service watching /metal/ on hostname: ifr0"
								},
								"StatusValue": "Success",
								"FirstUpdated": "2024-06-21T03:22:57.151985858Z",
								"LastUpdated": "2024-05-18T05:33:13.610186820Z"
							},
							"Subtasks": [
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-05-18T05:33:13.610186820Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/metal/md01",
										"Name": "/metal/md01",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": null,
										"LastUpdated": "2024-05-18T05:33:13.610186820Z"
									},
									"Subtasks": []
								}
							]
						},
						{
							"HighestStatus": "Success",
							"LastUpdated": "2024-05-18T05:33:19.573417096Z",
							"NumChildTasks": "0",
							"Task": {
								"Id": "/sled/harbor.system.marstb",
								"Name": "/sled/harbor.system.marstb",
								"Message": {
									"Level": "Info",
									"Message": "Sled service watching /metal/ on hostname: harbor.system.marstb"
								},
								"StatusValue": "Success",
								"FirstUpdated": "2024-06-21T03:22:57.151985858Z",
								"LastUpdated": "2024-05-18T05:33:19.573417096Z"
							},
							"Subtasks": [
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-05-18T05:33:19.573417096Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/metal/md01",
										"Name": "/metal/md01",
										"Message": {
											"Level": "Info",
											"Message": "node in state: On"
										},
										"StatusValue": "Success",
										"FirstUpdated": null,
										"LastUpdated": "2024-05-18T05:33:19.573417096Z"
									},
									"Subtasks": []
								}
							]
						}
					]
				},
				{
					"HighestStatus": "Success",
					"LastUpdated": "2024-06-21T03:22:59.287977425Z",
					"NumChildTasks": "2",
					"Task": {
						"Id": "test.hello.ctran/vmCreateOps",
						"Name": "vmCreateOps",
						"Message": {
							"Level": "Info",
							"Message": "Create virtual machines"
						},
						"StatusValue": "Success",
						"FirstUpdated": "2024-06-21T03:22:57.152189301Z",
						"LastUpdated": "2024-06-21T03:22:59.287977425Z"
					},
					"Subtasks": [
						{
							"HighestStatus": "Success",
							"LastUpdated": "2024-06-21T03:22:59.287977425Z",
							"NumChildTasks": "0",
							"Task": {
								"Id": "/mariner/md01",
								"Name": "/mariner/md01",
								"Message": {
									"Level": "Info",
									"Message": "Mariner service on hostname: md01"
								},
								"StatusValue": "Success",
								"FirstUpdated": "2024-06-21T03:22:57.152189301Z",
								"LastUpdated": "2024-06-21T03:22:59.287977425Z"
							},
							"Subtasks": [
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:22:58.458000142Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/vm/md01/ctran.hello.test.n0",
										"Name": "/vm/md01/ctran.hello.test.n0",
										"Message": {
											"Level": "Info",
											"Message": "VM created"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:22:57.158752545Z",
										"LastUpdated": "2024-06-21T03:22:58.458000142Z"
									},
									"Subtasks": []
								},
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:22:59.287977425Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/vm/md01/ctran.hello.test.n1",
										"Name": "/vm/md01/ctran.hello.test.n1",
										"Message": {
											"Level": "Info",
											"Message": "VM created"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:22:58.256371079Z",
										"LastUpdated": "2024-06-21T03:22:59.287977425Z"
									},
									"Subtasks": []
								}
							]
						}
					]
				},
				{
					"HighestStatus": "Success",
					"LastUpdated": "2024-06-21T03:22:57.890011864Z",
					"NumChildTasks": "14",
					"Task": {
						"Id": "test.hello.ctran/linkCreateOps",
						"Name": "linkCreateOps",
						"Message": {
							"Level": "Info",
							"Message": "Configure networking links"
						},
						"StatusValue": "Success",
						"FirstUpdated": "2024-06-21T03:22:57.156257685Z",
						"LastUpdated": "2024-06-21T03:22:57.890011864Z"
					},
					"Subtasks": [
						{
							"HighestStatus": "Success",
							"LastUpdated": "2024-06-21T03:22:57.361175622Z",
							"NumChildTasks": "0",
							"Task": {
								"Id": "/canopy/ifr1",
								"Name": "/canopy/ifr1",
								"Message": {
									"Level": "Info",
									"Message": "Canopy service watching /net/ifr1"
								},
								"StatusValue": "Success",
								"FirstUpdated": "2024-06-21T03:22:57.156257685Z",
								"LastUpdated": "2024-06-21T03:22:57.361175622Z"
							},
							"Subtasks": [
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-04-24T19:51:03.385204788Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/net/ifr1/peer/enp33s0f1np1",
										"Name": "/net/ifr1/peer/enp33s0f1np1",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": null,
										"LastUpdated": "2024-04-24T19:51:03.385204788Z"
									},
									"Subtasks": []
								},
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:22:57.361175622Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/net/ifr1/vtep/vtep124",
										"Name": "/net/ifr1/vtep/vtep124",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:22:57.158032387Z",
										"LastUpdated": "2024-06-21T03:22:57.361175622Z"
									},
									"Subtasks": []
								}
							]
						},
						{
							"HighestStatus": "Success",
							"LastUpdated": "2024-06-21T03:22:56.868772898Z",
							"NumChildTasks": "0",
							"Task": {
								"Id": "/canopy/il252r207s10-ileaf",
								"Name": "/canopy/il252r207s10-ileaf",
								"Message": {
									"Level": "Info",
									"Message": "Canopy service watching /net/il252r207s10-ileaf"
								},
								"StatusValue": "Success",
								"FirstUpdated": "2024-06-21T03:22:56.620372918Z",
								"LastUpdated": "2024-06-21T03:22:56.868772898Z"
							},
							"Subtasks": [
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-03-20T21:18:03.586065322Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/net/il252r207s10-ileaf/peer/swp52",
										"Name": "/net/il252r207s10-ileaf/peer/swp52",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": null,
										"LastUpdated": "2024-03-20T21:18:03.586065322Z"
									},
									"Subtasks": []
								},
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:22:56.868737301Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/net/il252r207s10-ileaf/trunk/swp2.856",
										"Name": "/net/il252r207s10-ileaf/trunk/swp2.856",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:22:56.620372918Z",
										"LastUpdated": "2024-06-21T03:22:56.868737301Z"
									},
									"Subtasks": []
								},
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:22:56.868772898Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/net/il252r207s10-ileaf/vtepw/vtep124",
										"Name": "/net/il252r207s10-ileaf/vtepw/vtep124",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:22:56.635358942Z",
										"LastUpdated": "2024-06-21T03:22:56.868772898Z"
									},
									"Subtasks": []
								}
							]
						},
						{
							"HighestStatus": "Success",
							"LastUpdated": "2024-06-21T03:22:57.363310255Z",
							"NumChildTasks": "0",
							"Task": {
								"Id": "/canopy/md00",
								"Name": "/canopy/md00",
								"Message": {
									"Level": "Info",
									"Message": "Canopy service watching /net/md00"
								},
								"StatusValue": "Success",
								"FirstUpdated": "2024-06-21T03:22:57.156257685Z",
								"LastUpdated": "2024-06-21T03:22:57.363310255Z"
							},
							"Subtasks": [
								{
									"HighestStatus": "Success",
									"LastUpdated": "2023-02-22T21:38:21.619371715Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/net/md00/peer/xleaf",
										"Name": "/net/md00/peer/xleaf",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": null,
										"LastUpdated": "2023-02-22T21:38:21.619371715Z"
									},
									"Subtasks": []
								},
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:22:57.363287155Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/net/md00/vtep/vtep125",
										"Name": "/net/md00/vtep/vtep125",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:22:57.159947456Z",
										"LastUpdated": "2024-06-21T03:22:57.363287155Z"
									},
									"Subtasks": []
								},
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:22:57.363310255Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/net/md00/vtep/vtep135",
										"Name": "/net/md00/vtep/vtep135",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:22:57.161574149Z",
										"LastUpdated": "2024-06-21T03:22:57.363310255Z"
									},
									"Subtasks": []
								}
							]
						},
						{
							"HighestStatus": "Success",
							"LastUpdated": "2024-06-21T03:22:57.890011864Z",
							"NumChildTasks": "0",
							"Task": {
								"Id": "/canopy/md01",
								"Name": "/canopy/md01",
								"Message": {
									"Level": "Info",
									"Message": "Canopy service watching /net/md01"
								},
								"StatusValue": "Success",
								"FirstUpdated": "2024-06-05T17:11:30.369431633Z",
								"LastUpdated": "2024-06-21T03:22:57.890011864Z"
							},
							"Subtasks": [
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-05T17:11:31.029767285Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/net/md01/peer/enp33s0f0np0",
										"Name": "/net/md01/peer/enp33s0f0np0",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-05T17:11:30.369431633Z",
										"LastUpdated": "2024-06-05T17:11:31.029767285Z"
									},
									"Subtasks": []
								},
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:22:57.889975604Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/net/md01/vlan/vlan856",
										"Name": "/net/md01/vlan/vlan856",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:22:57.158914027Z",
										"LastUpdated": "2024-06-21T03:22:57.889975604Z"
									},
									"Subtasks": []
								},
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:22:57.889996594Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/net/md01/vtep/vtep125",
										"Name": "/net/md01/vtep/vtep125",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:22:57.325494064Z",
										"LastUpdated": "2024-06-21T03:22:57.889996594Z"
									},
									"Subtasks": []
								},
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:22:57.890011864Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/net/md01/vtep/vtep135",
										"Name": "/net/md01/vtep/vtep135",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:22:57.509209796Z",
										"LastUpdated": "2024-06-21T03:22:57.890011864Z"
									},
									"Subtasks": []
								}
							]
						},
						{
							"HighestStatus": "Success",
							"LastUpdated": "2024-06-05T17:11:31.092788269Z",
							"NumChildTasks": "0",
							"Task": {
								"Id": "/canopy/xl252r207s8-xleaf",
								"Name": "/canopy/xl252r207s8-xleaf",
								"Message": {
									"Level": "Info",
									"Message": "Canopy service watching /net/xl252r207s8-xleaf"
								},
								"StatusValue": "Success",
								"FirstUpdated": "2024-06-21T03:22:57.156257685Z",
								"LastUpdated": "2024-06-05T17:11:31.092788269Z"
							},
							"Subtasks": [
								{
									"HighestStatus": "Success",
									"LastUpdated": "2023-02-22T21:38:21.569621350Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/net/xl252r207s8-xleaf/peer/md00",
										"Name": "/net/xl252r207s8-xleaf/peer/md00",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": null,
										"LastUpdated": "2023-02-22T21:38:21.569621350Z"
									},
									"Subtasks": []
								},
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-05T17:11:31.092788269Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/net/xl252r207s8-xleaf/peer/swp2",
										"Name": "/net/xl252r207s8-xleaf/peer/swp2",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": null,
										"LastUpdated": "2024-06-05T17:11:31.092788269Z"
									},
									"Subtasks": []
								}
							]
						}
					]
				},
				{
					"HighestStatus": "Success",
					"LastUpdated": "2024-06-21T03:23:01.156491862Z",
					"NumChildTasks": "14",
					"Task": {
						"Id": "test.hello.ctran/danceCreateOps",
						"Name": "danceCreateOps",
						"Message": {
							"Level": "Info",
							"Message": "Allocate ip addresses to materialization resources"
						},
						"StatusValue": "Success",
						"FirstUpdated": "2024-06-21T03:22:57.157896264Z",
						"LastUpdated": "2024-06-21T03:23:01.156491862Z"
					},
					"Subtasks": [
						{
							"HighestStatus": "Success",
							"LastUpdated": "2024-06-21T03:23:01.156491862Z",
							"NumChildTasks": "0",
							"Task": {
								"Id": "/dance.entries/dance.dns4",
								"Name": "/dance.entries/dance.dns4",
								"Message": {
									"Level": "Info",
									"Message": "Dance service watching /dance/test.hello.ctran/dns4/test.hello.ctran"
								},
								"StatusValue": "Success",
								"FirstUpdated": "2024-06-21T03:22:57.157896264Z",
								"LastUpdated": "2024-06-21T03:23:01.156491862Z"
							},
							"Subtasks": [
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:23:01.156447081Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/dance/test.hello.ctran/dns4/test.hello.ctran/foundry.test.hello.ctran",
										"Name": "/dance/test.hello.ctran/dns4/test.hello.ctran/foundry.test.hello.ctran",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:23:01.153420914Z",
										"LastUpdated": "2024-06-21T03:23:01.156447081Z"
									},
									"Subtasks": []
								},
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:23:01.156457462Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/dance/test.hello.ctran/dns4/test.hello.ctran/images.system.marstb",
										"Name": "/dance/test.hello.ctran/dns4/test.hello.ctran/images.system.marstb",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:23:01.153911565Z",
										"LastUpdated": "2024-06-21T03:23:01.156457462Z"
									},
									"Subtasks": []
								},
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:23:01.156453952Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/dance/test.hello.ctran/dns4/test.hello.ctran/moactl.infra.test.hello.ctran",
										"Name": "/dance/test.hello.ctran/dns4/test.hello.ctran/moactl.infra.test.hello.ctran",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:23:01.153633378Z",
										"LastUpdated": "2024-06-21T03:23:01.156453952Z"
									},
									"Subtasks": []
								},
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:23:01.156459792Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/dance/test.hello.ctran/dns4/test.hello.ctran/n0-10-0-1-1.exp.test.hello.ctran",
										"Name": "/dance/test.hello.ctran/dns4/test.hello.ctran/n0-10-0-1-1.exp.test.hello.ctran",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:23:01.154053758Z",
										"LastUpdated": "2024-06-21T03:23:01.156459792Z"
									},
									"Subtasks": []
								},
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:23:01.156461922Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/dance/test.hello.ctran/dns4/test.hello.ctran/n0.exp.test.hello.ctran",
										"Name": "/dance/test.hello.ctran/dns4/test.hello.ctran/n0.exp.test.hello.ctran",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:23:01.154190931Z",
										"LastUpdated": "2024-06-21T03:23:01.156461922Z"
									},
									"Subtasks": []
								},
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:23:01.156467852Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/dance/test.hello.ctran/dns4/test.hello.ctran/n0.infra.test.hello.ctran",
										"Name": "/dance/test.hello.ctran/dns4/test.hello.ctran/n0.infra.test.hello.ctran",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:23:01.154436396Z",
										"LastUpdated": "2024-06-21T03:23:01.156467852Z"
									},
									"Subtasks": []
								},
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:23:01.156465572Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/dance/test.hello.ctran/dns4/test.hello.ctran/n1-10-0-1-2.exp.test.hello.ctran",
										"Name": "/dance/test.hello.ctran/dns4/test.hello.ctran/n1-10-0-1-2.exp.test.hello.ctran",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:23:01.154336684Z",
										"LastUpdated": "2024-06-21T03:23:01.156465572Z"
									},
									"Subtasks": []
								},
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:23:01.156470692Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/dance/test.hello.ctran/dns4/test.hello.ctran/n1.exp.test.hello.ctran",
										"Name": "/dance/test.hello.ctran/dns4/test.hello.ctran/n1.exp.test.hello.ctran",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:23:01.154526978Z",
										"LastUpdated": "2024-06-21T03:23:01.156470692Z"
									},
									"Subtasks": []
								},
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:23:01.156488692Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/dance/test.hello.ctran/dns4/test.hello.ctran/n1.infra.test.hello.ctran",
										"Name": "/dance/test.hello.ctran/dns4/test.hello.ctran/n1.infra.test.hello.ctran",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:23:01.154700882Z",
										"LastUpdated": "2024-06-21T03:23:01.156488692Z"
									},
									"Subtasks": []
								},
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:23:01.156491862Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/dance/test.hello.ctran/dns4/test.hello.ctran/podetcd.test.hello.ctran",
										"Name": "/dance/test.hello.ctran/dns4/test.hello.ctran/podetcd.test.hello.ctran",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:23:01.154801434Z",
										"LastUpdated": "2024-06-21T03:23:01.156491862Z"
									},
									"Subtasks": []
								},
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:23:01.156481652Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/dance/test.hello.ctran/dns4/test.hello.ctran/sled.system.marstb",
										"Name": "/dance/test.hello.ctran/dns4/test.hello.ctran/sled.system.marstb",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:23:01.154624830Z",
										"LastUpdated": "2024-06-21T03:23:01.156481652Z"
									},
									"Subtasks": []
								}
							]
						},
						{
							"HighestStatus": "Success",
							"LastUpdated": "2024-06-21T03:23:01.153844463Z",
							"NumChildTasks": "0",
							"Task": {
								"Id": "/dance.entries/dance.entry",
								"Name": "/dance.entries/dance.entry",
								"Message": {
									"Level": "Info",
									"Message": "Dance service watching /dance/test.hello.ctran/entry/test.hello.ctran"
								},
								"StatusValue": "Success",
								"FirstUpdated": "2024-06-21T03:22:57.157896264Z",
								"LastUpdated": "2024-06-21T03:23:01.153844463Z"
							},
							"Subtasks": [
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:23:01.153834403Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/dance/test.hello.ctran/entry/test.hello.ctran/3a:ab:13:fb:ca:af",
										"Name": "/dance/test.hello.ctran/entry/test.hello.ctran/3a:ab:13:fb:ca:af",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:23:01.151132052Z",
										"LastUpdated": "2024-06-21T03:23:01.153834403Z"
									},
									"Subtasks": []
								},
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:23:01.153844463Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/dance/test.hello.ctran/entry/test.hello.ctran/42:59:50:54:6c:68",
										"Name": "/dance/test.hello.ctran/entry/test.hello.ctran/42:59:50:54:6c:68",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:23:01.151429169Z",
										"LastUpdated": "2024-06-21T03:23:01.153844463Z"
									},
									"Subtasks": []
								}
							]
						},
						{
							"HighestStatus": "Success",
							"LastUpdated": "2024-06-21T03:23:01.150130770Z",
							"NumChildTasks": "0",
							"Task": {
								"Id": "/dance.network/dance.network",
								"Name": "/dance.network/dance.network",
								"Message": {
									"Level": "Info",
									"Message": "Dance service watching /dance/test.hello.ctran/network/test.hello.ctran"
								},
								"StatusValue": "Success",
								"FirstUpdated": "2024-06-21T03:22:57.157896264Z",
								"LastUpdated": "2024-06-21T03:23:01.150130770Z"
							},
							"Subtasks": [
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:23:01.150130770Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/dance/test.hello.ctran/network/test.hello.ctran",
										"Name": "/dance/test.hello.ctran/network/test.hello.ctran",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:23:01.148648937Z",
										"LastUpdated": "2024-06-21T03:23:01.150130770Z"
									},
									"Subtasks": []
								}
							]
						}
					]
				},
				{
					"HighestStatus": "Success",
					"LastUpdated": "2024-06-21T03:23:01.379573105Z",
					"NumChildTasks": "1",
					"Task": {
						"Id": "test.hello.ctran/createInfrapod",
						"Name": "createInfrapod",
						"Message": {
							"Level": "Info",
							"Message": "Create mtz infrapod"
						},
						"StatusValue": "Success",
						"FirstUpdated": "2024-06-21T03:22:57.157945985Z",
						"LastUpdated": "2024-06-21T03:23:01.379573105Z"
					},
					"Subtasks": [
						{
							"HighestStatus": "Success",
							"LastUpdated": "2024-06-21T03:23:01.379573105Z",
							"NumChildTasks": "0",
							"Task": {
								"Id": "/infrapod/ifr1",
								"Name": "/infrapod/ifr1",
								"Message": {
									"Level": "Info",
									"Message": "Manage infrapods on ifr1"
								},
								"StatusValue": "Success",
								"FirstUpdated": "2024-06-21T03:22:57.157945985Z",
								"LastUpdated": "2024-06-21T03:23:01.379573105Z"
							},
							"Subtasks": [
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:23:01.379573105Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/infrapod/test.hello.ctran/ifr1",
										"Name": "/infrapod/test.hello.ctran/ifr1",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:22:57.158085518Z",
										"LastUpdated": "2024-06-21T03:23:01.379573105Z"
									},
									"Subtasks": []
								}
							]
						}
					]
				},
				{
					"HighestStatus": "Success",
					"LastUpdated": "2024-06-21T03:23:03.451612003Z",
					"NumChildTasks": "2",
					"Task": {
						"Id": "test.hello.ctran/wireguardOps",
						"Name": "wireguardOps",
						"Message": {
							"Level": "Info",
							"Message": "Facility-side wireguard operations"
						},
						"StatusValue": "Success",
						"FirstUpdated": "2024-06-21T03:22:57.157955666Z",
						"LastUpdated": "2024-06-21T03:23:03.451612003Z"
					},
					"Subtasks": [
						{
							"HighestStatus": "Success",
							"LastUpdated": "2024-06-21T03:23:03.451612003Z",
							"NumChildTasks": "0",
							"Task": {
								"Id": "/wireguard/wireguardSync.ifr1",
								"Name": "/wireguard/wireguardSync.ifr1",
								"Message": {
									"Level": "Info",
									"Message": "Synchronize site wireguard state with portal"
								},
								"StatusValue": "Success",
								"FirstUpdated": "2024-06-21T03:22:57.157955666Z",
								"LastUpdated": "2024-06-21T03:23:03.451612003Z"
							},
							"Subtasks": [
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:23:03.451612003Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/wg/if/test.hello.ctran/ifr1",
										"Name": "/wg/if/test.hello.ctran/ifr1",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:23:02.175136252Z",
										"LastUpdated": "2024-06-21T03:23:03.451612003Z"
									},
									"Subtasks": []
								}
							]
						},
						{
							"HighestStatus": "Success",
							"LastUpdated": "2024-06-21T03:23:02.379291510Z",
							"NumChildTasks": "0",
							"Task": {
								"Id": "/wireguard/wireguardIF.ifr1",
								"Name": "/wireguard/wireguardIF.ifr1",
								"Message": {
									"Level": "Info",
									"Message": "Manage wireguard interfaces"
								},
								"StatusValue": "Success",
								"FirstUpdated": "2024-06-21T03:22:57.157955666Z",
								"LastUpdated": "2024-06-21T03:23:02.379291510Z"
							},
							"Subtasks": [
								{
									"HighestStatus": "Success",
									"LastUpdated": "2024-06-21T03:23:02.379291510Z",
									"NumChildTasks": "0",
									"Task": {
										"Id": "/wg/ifreq/test.hello.ctran/ifr1",
										"Name": "/wg/ifreq/test.hello.ctran/ifr1",
										"Message": {
											"Level": "Info",
											"Message": "task complete"
										},
										"StatusValue": "Success",
										"FirstUpdated": "2024-06-21T03:22:57.421134076Z",
										"LastUpdated": "2024-06-21T03:23:02.379291510Z"
									},
									"Subtasks": []
								}
							]
						}
					]
				}
			]
		}
	]
}
`

func TestOutput(t *testing.T) {
	tw := tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)
	a := assert.New(t)

	tt := new(portal.TaskTree)

	err := protojson.Unmarshal([]byte(tree), tt)
	a.Nil(err)
	WriteTaskForest(tt, 0, true, tw)
}
