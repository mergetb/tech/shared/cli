package portal

import (
	"fmt"
	"strings"
	"text/tabwriter"
	"time"

	mapset "github.com/deckarep/golang-set/v2"
	"github.com/schollz/progressbar/v3"

	"gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/tech/shared/cli"
)

type tasktreeres struct {
	results []*cli.Table
	printed mapset.Set[*portal.TaskTree]
}

// this is used to get the variables that we need for the progress bar:
//   - successes (how many tasks were complete)
//   - total (how many total tasks are there)
//   - desc (a description of the "currently" executing task)
//
// successes and total are counted from the actual tasks, i.e.,
// the tasks at the bottom of the task tree, as things higher up are
// there to just organize tasks
//
// we just take the description of the first unsuccessful task for the progress bar,
// as it's cleaner to just show the "currently" executing task,
// even if multiple tasks are being executed concurrently
func CountTaskTreeProgress(current_node *portal.TaskTree) (successes, total int, desc string) {
	if current_node == nil {
		return
	}

	sts := current_node.GetSubtasks()

	// we're at the bottom
	if len(sts) == 0 {
		t := current_node.GetTask()

		total += 1

		if current_node.GetTask().GetStatusValue() == portal.TaskStatus_Success {
			successes += 1
		} else {
			st := t.GetStatusValue()
			color := cli.ItemStatusToColorDarkMode[TaskStatusToCliLevel[st]]

			desc = strings.TrimSpace(color(fmt.Sprintf("%s: %s",
				t.GetName(),
				t.GetMessage().GetMessage(),
			)))
		}
	}

	for _, st := range sts {
		s, t, d := CountTaskTreeProgress(st)
		successes += s
		total += t

		if len(desc) == 0 {
			desc = d
		}
	}

	return
}

func TaskTreeToProgressBar(status *portal.TaskTree) (pb *progressbar.ProgressBar, err error) {
	if status == nil {
		return
	}

	successes, total, desc := CountTaskTreeProgress(status)

	pb = progressbar.NewOptions(
		total,
		progressbar.OptionClearOnFinish(),
		progressbar.OptionEnableColorCodes(cli.ColorEnabled()),
		progressbar.OptionSetDescription(desc),
		progressbar.OptionSetElapsedTime(true),
		progressbar.OptionSetPredictTime(true),
		progressbar.OptionSetRenderBlankState(true),
		progressbar.OptionShowCount(),
		progressbar.OptionShowElapsedTimeOnFinish(),
	)

	err = pb.Set(successes)

	return
}

func TaskTreeUpdateProgressBar(status *portal.TaskTree, pb *progressbar.ProgressBar) (err error) {
	if status == nil || pb == nil {
		return
	}

	successes, total, desc := CountTaskTreeProgress(status)

	pb.ChangeMax(total)

	err = pb.Set(successes)

	pb.Describe(desc)

	if status.HighestStatus == portal.TaskStatus_Success {
		err = pb.Finish()
	}

	return
}

// displaying the task tree is actually kind of hard.
// it was designed to have user-selecting levels of detail,
// in which you look at the top of the tree for a summary,
// but look at the bottom of the tree for specific details
//
// in the webgui, this is easy with interactivity,
// but in the cli, this is surprisingly non-trivial
// my best solution for right now is to have the user
// specify the depth/amount of information they want.
// a depth closer to 0 is closer to a summary,
// while a higher depth means the user wants more specific details
//
// takes in a task tree and a target depth
// and returns a table consisting of entries from that specific depth
// this also supports negative numbers, where "-N" means the depth from the bottom
// for example, "-1" means all of leaves of the tree aka. the actual tasking
func TaskTreeToTables(tt *portal.TaskTree, depth int) []*cli.Table {
	var res tasktreeres
	res.printed = mapset.NewSet[*portal.TaskTree]()

	taskTreeToTableHelper(tt, nil, depth, &res)

	return res.results
}

// we recursively descend the tree:
//   - current_node is what we're currently visiting
//   - parent_path is the path we took to get here (this is kept for the purposes of printing it later on)
//   - depth is how much further we need to descend before printing
//   - res contains the resulting table and nodes that we already visited (so we don't revisit them)
//
// it returns how far the node can be considered to be from the bottom.
// this is a mapset, as if the left and right path themselves are of different lengths,
// then the node could be considered to be a varying amount from the bottom, depending on which path you take
// this is only used when depth is negative, in which we need to first descend the tree to figure out how deep it goes,
// then we can figure out how far we need to be for the resulting table
//
// the basic algorithm is:
//   - base case: depth is 0/1, just print the current node or the children of the current node
//   - depth > 2: descend and decrement depth
//   - depth < 0: descend until we find the bottom, then as we come back up, if -depth == depth from bottom, print it
//
// there's edge cases in case you cannot descend/ascend further,
// in which case, depth becomes clamped to the limits of the tree instead of printing nothing
func taskTreeToTableHelper(current_node *portal.TaskTree, parent_path []*portal.TaskTree, depth int, res *tasktreeres) mapset.Set[int] {

	// nothing to do
	if current_node == nil {
		return nil
	}

	// we're right before the children we want to print,
	// so create a table and print the children
	if depth == 0 || depth == 1 {

		// don't bother printing if we've already have
		if res.printed.ContainsOne(current_node) {
			return nil
		}

		res.printed.Add(current_node)

		var sks []string
		var sks_levels []cli.TableItemStatus

		parents := append(parent_path, current_node)
		pnames := make([]string, len(parents))
		for i, p := range parents {
			pnames[i] = p.GetTask().Name
		}

		table := new(cli.Table)
		table.Title = fmt.Sprintf("%s: %s", strings.Join(pnames, "|"), current_node.GetTask().GetMessage().GetMessage())
		table.TitleColor = ReconcileColors[TaskStatusToCliLevel[current_node.GetHighestStatus()]]

		sts := current_node.GetSubtasks()
		// if we somehow got here,
		// it means that a depth of zero was passed initially
		// so, just have the children be the same node
		if depth == 0 {
			sts = []*portal.TaskTree{current_node}
		}

		// we're at the bottom, print the parent
		if len(sts) == 0 {
			// no parents, just treat this as a 0
			if len(parent_path) == 0 {
				return taskTreeToTableHelper(current_node, parent_path, 0, res)
			}

			parent := parent_path[len(parent_path)-1]
			prev_parents := parent_path[:len(parent_path)-1]
			return taskTreeToTableHelper(parent, prev_parents, 1, res)
		}

		for _, st := range sts {
			sks = append(sks, st.GetTask().GetName())
			sks_levels = append(sks_levels, TaskStatusToCliLevel[st.GetHighestStatus()])

			t := st.GetTask()

			level := TaskStatusToCliLevel[st.GetHighestStatus()]
			cur_row := []cli.TableItem{
				{
					Key:    "Parent",
					Value:  current_node.Task.GetName(),
					Status: level,
				},
				{
					Key:    "Name",
					Value:  t.GetName(),
					Status: level,
				},
				{
					Key:    "Desc",
					Value:  t.GetMessage().GetMessage(),
					Status: TaskMessageLevelToCliLevel[t.GetMessage().GetLevel()],
				},
				{
					Key:    "Status",
					Value:  t.StatusValue,
					Status: level,
				},
				{
					Key:    "Creation",
					Value:  t.GetFirstUpdated().AsTime().Format(time.UnixDate),
					Status: level,
				},
				{
					Key:    "Last Updated",
					Value:  t.GetLastUpdated().AsTime().Format(time.UnixDate),
					Status: level,
				},
			}

			table.Rows = append(table.Rows, cur_row)

		}

		res.results = append(res.results, table)

		return nil
	}

	if depth >= 2 {

		// we're at the bottom, just print the parent
		if len(current_node.GetSubtasks()) == 0 {

			// no parents, just treat this as a 0
			if len(parent_path) == 0 {
				return taskTreeToTableHelper(current_node, parent_path, 0, res)
			}

			parent := parent_path[len(parent_path)-1]
			prev_parents := parent_path[:len(parent_path)-1]
			return taskTreeToTableHelper(parent, prev_parents, 1, res)

		} else {
			for _, st := range current_node.GetSubtasks() {
				parents := append(parent_path[:], current_node)
				taskTreeToTableHelper(st, parents, depth-1, res)
			}
		}

		return nil
	}

	// we'll need to find the bottom of the tree
	if depth < 0 {

		// we're at the bottom of the tree
		if len(current_node.GetSubtasks()) == 0 {

			// no parents to backtrack, so just print as is
			if len(parent_path) == 0 {
				return taskTreeToTableHelper(current_node, parent_path, 0, res)
			}

			// return the caller's distance from the bottom: here, it's 1
			return mapset.NewSet(1)
		} else {
			// we're not at the bottom of the tree, but we'll find the distance from the bottom
			depths := mapset.NewSet[int]()
			max_d := 0

			for _, st := range current_node.GetSubtasks() {
				parents := append(parent_path[:], current_node)
				ds := taskTreeToTableHelper(st, parents, depth, res)

				for d := range ds.Iter() {
					// if at least 1 depth is the correct distance from the bottom
					if d == -depth {
						taskTreeToTableHelper(current_node, parent_path, 1, res)
					}

					if d > max_d {
						max_d = d
					}

					// add 1 to the depths, as caller is 1 further away from the bottom
					depths.Add(d + 1)
				}
			}

			// if we're at the top and the requested negative depth is too high, also print it
			if len(parent_path) == 0 && -depth > max_d {
				taskTreeToTableHelper(current_node, parent_path, 0, res)
			}

			return depths
		}

	}

	return nil
}

func WriteTaskForest(tt *portal.TaskTree, depth int, condensed bool, tw *tabwriter.Writer) error {

	if tt == nil {
		return nil
	}

	tables := TaskTreeToTables(tt, depth)

	if condensed {
		do_once := true
		var mega_table cli.Table

		for _, table := range tables {
			if do_once {
				mega_table.Rows = append(mega_table.Rows, table.Rows[0])
			}

			mega_table.Rows = append(mega_table.Rows, table.Rows[1:]...)
		}

		// only 1 line of data, skip the first column of parent data
		if len(mega_table.Rows) == 1 {
			for i := range mega_table.Rows {
				mega_table.Rows[i] = mega_table.Rows[i][1:]
			}
		}

		tw.Flush()
		fmt.Fprint(tw, mega_table.ColumnsToString())
		fmt.Fprint(tw, mega_table.RowsToString())
		tw.Flush()

	} else {
		for i, table := range tables {
			tw.Flush()
			fmt.Fprint(tw, table.TitleToString())
			tw.Flush()

			// skip the first column of parent data
			for i := range table.Rows {
				table.Rows[i] = table.Rows[i][1:]
			}

			fmt.Fprint(tw, table.ColumnsToString())
			fmt.Fprint(tw, table.RowsToString())
			tw.Flush()

			if i != len(tables)-1 {
				fmt.Println()
			}
		}

	}

	tw.Flush()

	return nil

}
