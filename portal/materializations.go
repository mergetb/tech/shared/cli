package portal

import (
	"fmt"
	"time"

	"gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/tech/shared/cli"
)

func ListMaterializationsToTable(mzs []*portal.MaterializationSummary, statuses []*portal.TaskSummary) *cli.Table {
	table := new(cli.Table)
	table.Title = "List materializations"

	for i, mz := range mzs {

		status := statuses[i]
		level := TaskStatusToCliLevel[status.HighestStatus]

		mzid := fmt.Sprintf("%s.%s.%s", mz.Rid, mz.Eid, mz.Pid)

		cur_row := []cli.TableItem{
			{
				Key:    "Materialization",
				Value:  mzid,
				Status: level,
			},
			{
				Key:    "Metal",
				Value:  mz.NumMetal,
				Status: level,
			},
			{
				Key:    "VMs",
				Value:  mz.NumVirtual,
				Status: level,
			},
			{
				Key:    "Links",
				Value:  mz.NumLinks,
				Status: level,
			},
			{
				Key:    "Created",
				Value:  status.GetFirstUpdated().AsTime().Format(time.UnixDate),
				Status: level,
			},
			{
				Key:    "Last Updated",
				Value:  status.GetLastUpdated().AsTime().Format(time.UnixDate),
				Status: level,
			},
			{
				Key:    "Status",
				Value:  status.HighestStatus,
				Status: level,
			},
		}

		table.Rows = append(table.Rows, cur_row)
	}

	return table
}
