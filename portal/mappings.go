package portal

import (
	"gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/tech/shared/cli"
)

var (
	TaskMessageLevelToCliLevel = map[portal.TaskMessage_Type]cli.TableItemStatus{
		portal.TaskMessage_Undefined: cli.ItemStatusNone,
		portal.TaskMessage_Error:     cli.ItemStatusError,
		portal.TaskMessage_Warning:   cli.ItemStatusWarn,
		portal.TaskMessage_Info:      cli.ItemStatusOK,
		portal.TaskMessage_Debug:     cli.ItemStatusNone,
		portal.TaskMessage_Trace:     cli.ItemStatusNone,
	}

	TaskStatusToCliLevel = map[portal.TaskStatus_StatusType]cli.TableItemStatus{
		portal.TaskStatus_Undefined:    cli.ItemStatusNone,
		portal.TaskStatus_Processing:   cli.ItemStatusWarn,
		portal.TaskStatus_Error:        cli.ItemStatusError,
		portal.TaskStatus_Success:      cli.ItemStatusOK,
		portal.TaskStatus_Pending:      cli.ItemStatusWarn,
		portal.TaskStatus_Deleted:      cli.ItemStatusOK,
		portal.TaskStatus_Unresponsive: cli.ItemStatusWarn,
	}

	ReconcileColors = map[cli.TableItemStatus]cli.Color{
		cli.ItemStatusNone:  cli.White,
		cli.ItemStatusWarn:  cli.HiYellow,
		cli.ItemStatusError: cli.HiRed,
		cli.ItemStatusOK:    cli.Blue,
	}
)
