package reconcile

import (
	"strings"

	rec "gitlab.com/mergetb/tech/reconcile"
	"gitlab.com/mergetb/tech/shared/cli"
)

func TaskMessagesToItem(status rec.TaskStatus_StatusType, messages []*rec.TaskMessage) cli.TableItem {

	items := make([]string, 0, len(messages))
	statuses := make([]cli.TableItemStatus, 0, len(messages))
	for _, s := range messages {
		items = append(items, strings.TrimSpace(s.Message))
		statuses = append(statuses, TaskMessageLevelToCliLevel[s.Level])
	}

	return cli.TableItem{
		Key:         "Messages",
		Value:       strings.Join(items, "\n"),
		Status:      TaskStatusToCliLevel[status],
		MultiStatus: statuses,
	}
}
