package reconcile

import (
	"time"

	rec "gitlab.com/mergetb/tech/reconcile"
	"gitlab.com/mergetb/tech/shared/cli"
)

func TaskStatusToTable(ts *rec.TaskStatus, parentpath ...string) *cli.Table {
	if ts == nil {
		return nil
	}

	timestamp := ts.GetWhen().AsTime().Format(time.UnixDate)
	if ts.GetWhen() == nil {
		timestamp = ""
	}

	t := new(cli.Table)
	t.Title = ts.GetTaskKey()

	status := TaskStatusToCliLevel[ts.GetCurrentStatus()]

	t.Rows = [][]cli.TableItem{{
		{
			Key:    "TaskKey",
			Value:  ts.GetTaskKey(),
			Status: status,
		},
		{
			Key:    "Reconciler",
			Value:  ts.GetReconcilerPath(),
			Status: status,
		},
		cli.TableItem{
			Key:    "Parent",
			Value:  rec.JoinParentPath(parentpath...),
			Status: status,
		},
		// {
		// 	Key:    "Desc",
		// 	Value:  ts.GetDesc(),
		// 	Status: status,
		// },
		// {
		// 	Key:    "SubTasks",
		// 	Value:  ts.GetSubtaskKeys(),
		// 	Status: status,
		// },
		{
			Key:    "Status",
			Value:  ts.GetCurrentStatus(),
			Status: status,
		},
		// {
		// 	Key:    "LastStatus",
		// 	Value:  ts.GetLastStatus(),
		// 	Status: status,
		// },
		// {
		// 	Key:    "Version",
		// 	Value:  ts.GetTaskVersion(),
		// 	Status: status,
		// },
		{
			Key:    "When",
			Value:  timestamp,
			Status: status,
		},
		TaskMessagesToItem(ts.GetCurrentStatus(), ts.GetMessages()),
	}}

	return t
}
