package reconcile

import (
	rec "gitlab.com/mergetb/tech/reconcile"
	"gitlab.com/mergetb/tech/shared/cli"
)

var (
	TaskMessageLevelToCliLevel = map[rec.TaskMessage_Type]cli.TableItemStatus{
		rec.TaskMessage_Undefined: cli.ItemStatusNone,
		rec.TaskMessage_Error:     cli.ItemStatusError,
		rec.TaskMessage_Warning:   cli.ItemStatusWarn,
		rec.TaskMessage_Info:      cli.ItemStatusOK,
		rec.TaskMessage_Debug:     cli.ItemStatusNone,
		rec.TaskMessage_Trace:     cli.ItemStatusNone,
	}

	TaskStatusToCliLevel = map[rec.TaskStatus_StatusType]cli.TableItemStatus{
		rec.TaskStatus_Undefined:    cli.ItemStatusNone,
		rec.TaskStatus_Processing:   cli.ItemStatusWarn,
		rec.TaskStatus_Error:        cli.ItemStatusError,
		rec.TaskStatus_Success:      cli.ItemStatusOK,
		rec.TaskStatus_Pending:      cli.ItemStatusWarn,
		rec.TaskStatus_Deleted:      cli.ItemStatusOK,
		rec.TaskStatus_Unresponsive: cli.ItemStatusWarn,
	}

	ReconcileColors = map[cli.TableItemStatus]cli.Color{
		cli.ItemStatusNone:  cli.White,
		cli.ItemStatusWarn:  cli.HiYellow,
		cli.ItemStatusError: cli.HiRed,
		cli.ItemStatusOK:    cli.Blue,
	}
)
