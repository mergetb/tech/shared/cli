package reconcile

import (
	"os"
	"testing"
	"text/tabwriter"

	"github.com/stretchr/testify/assert"

	rec "gitlab.com/mergetb/tech/reconcile"
	"google.golang.org/protobuf/encoding/protojson"
)

const forest1 = `{
	"Goal":  {
	  "SelfKey":  "/keys/murphy",
	  "Name":  "/keys/murphy",
	  "Desc":  "Create murphy credentials",
	  "Type":  "Supertask",
	  "SelfVersion":  "1",
	  "Creation":  "2022-09-23T17:32:42.669698336Z",
	  "When":  "2022-09-23T17:32:42.669699217Z",
	  "Subkeys":  [
		"/keys/murphy"
	  ]
	},
	"HighestStatus":  "Success",
	"LastUpdated":  "2022-09-23T17:32:42.810489135Z",
	"NumChildTasks":  "1",
	"Subtasks":  [
	  {
		"Task":  {
		  "ReconcilerManager":  "mergefs",
		  "ReconcilerName":  "userkeys",
		  "Desc":  "Place user keys in $HOME/.ssh",
		  "LastStatus":  "Success",
		  "SelfVersion":  "3",
		  "TaskVersion":  "1",
		  "PrevValue":  "bXVycGh5",
		  "When":  "2022-09-23T17:32:42.810489135Z",
		  "TaskKey":  "/keys/murphy",
		  "Messages":  [
			{
			  "Level":  "Info",
			  "Message":  "task complete"
			}
		  ],
		  "CurrentStatus":  "Success"
		},
		"HighestStatus":  "Success",
		"LastUpdated":  "2022-09-23T17:32:42.810489135Z"
	  }
	]
  }
`

const forest2 = `{
	"Goal":  {
	  "SelfKey":  "/users/murphy",
	  "Name":  "/users/murphy",
	  "Desc":  "Create user murphy",
	  "Type":  "Supertask",
	  "SelfVersion":  "1",
	  "Creation":  "2022-09-23T17:32:42.661138465Z",
	  "When":  "2022-09-23T17:32:42.661141611Z",
	  "Subkeys":  [
		"/users/murphy"
	  ]
	},
	"HighestStatus":  "Success",
	"LastUpdated":  "2022-09-23T17:32:42.810489135Z",
	"NumChildTasks":  "3",
	"Subtasks":  [
	  {
		"Task":  {
		  "ReconcilerManager":  "cred",
		  "ReconcilerName":  "createuserkeys",
		  "Desc":  "Create ssh creds",
		  "LastStatus":  "Success",
		  "SelfVersion":  "4",
		  "TaskVersion":  "1",
		  "PrevValue":  "bXVycGh5",
		  "When":  "2022-09-23T17:32:42.748800520Z",
		  "TaskKey":  "/users/murphy",
		  "SubtaskKeys":  [
			"/keys/murphy"
		  ],
		  "Messages":  [
			{
			  "Level":  "Info",
			  "Message":  "task complete"
			}
		  ],
		  "CurrentStatus":  "Success"
		},
		"HighestStatus":  "Success",
		"LastUpdated":  "2022-09-23T17:32:42.810489135Z",
		"NumChildTasks":  "1",
		"Subtasks":  [
		  {
			"Task":  {
			  "ReconcilerManager":  "mergefs",
			  "ReconcilerName":  "userkeys",
			  "Desc":  "Place user keys in $HOME/.ssh",
			  "LastStatus":  "Success",
			  "SelfVersion":  "3",
			  "TaskVersion":  "1",
			  "PrevValue":  "bXVycGh5",
			  "When":  "2022-09-23T17:32:42.810489135Z",
			  "TaskKey":  "/keys/murphy",
			  "Messages":  [
				{
				  "Level":  "Info",
				  "Message":  "task complete"
				}
			  ],
			  "CurrentStatus":  "Success"
			},
			"HighestStatus":  "Success",
			"LastUpdated":  "2022-09-23T17:32:42.810489135Z"
		  }
		]
	  },
	  {
		"Task":  {
		  "ReconcilerManager":  "mergefs",
		  "ReconcilerName":  "userhome",
		  "LastStatus":  "Success",
		  "SelfVersion":  "3",
		  "TaskVersion":  "1",
		  "PrevValue":  "bXVycGh5",
		  "When":  "2022-09-23T17:32:42.696459571Z",
		  "TaskKey":  "/users/murphy",
		  "Messages":  [
			{
			  "Level":  "Info",
			  "Message":  "task complete"
			}
		  ],
		  "CurrentStatus":  "Success"
		},
		"HighestStatus":  "Success",
		"LastUpdated":  "2022-09-23T17:32:42.696459571Z"
	  }
	]
  }
`

const forest3 = `{
	"Goal": {
	  "SelfKey": "/users/murphy",
	  "Name": "/users/murphy",
	  "Desc": "Create user murphy",
	  "Type": "Supergoal",
	  "Creation": "2024-02-21T22:55:17.781866492Z",
	  "When": "2024-02-21T22:55:17.781250793Z",
	  "Subtasks": [
		{
		  "Task": "/users/murphy",
		  "ReconcilerManager": "mergefs",
		  "ReconcilerName": "userhome"
		},
		{
		  "Task": "/keys/murphy",
		  "ReconcilerManager": "mergefs",
		  "ReconcilerName": "userkeys"
		},
		{
		  "Task": "/users/murphy",
		  "ReconcilerManager": "cred",
		  "ReconcilerName": "createuserkeys"
		}
	  ]
	},
	"HighestStatus": "Pending",
	"LastUpdated": "2024-02-21T22:55:19.782265841Z",
	"NumChildTasks": "3",
	"Subgoals": [
	  {
		"Goal": {
		  "Name": "/mergefs/userkeys",
		  "Desc": "Pending keys",
		  "Type": "Supertask",
		  "Creation": "2024-02-21T22:55:17.781866492Z",
		  "When": "2024-02-21T22:55:17.781250793Z"
		},
		"HighestStatus": "Pending",
		"LastUpdated": "2024-02-21T22:55:19.782264448Z",
		"Subtasks": [
		  {
			"Task": {
			  "ReconcilerManager": "mergefs",
			  "ReconcilerName": "userkeys",
			  "Desc": "Pending keys",
			  "LastStatus": "Pending",
			  "When": "2024-02-21T22:55:19.782264448Z",
			  "TaskKey": "/keys/murphy",
			  "Messages": [
				{
				  "Level": "Warning",
				  "Message": "waiting for a reconciler to process this key..."
				}
			  ],
			  "CurrentStatus": "Pending"
			},
			"HighestStatus": "Pending",
			"LastUpdated": "2024-02-21T22:55:19.782264448Z"
		  }
		]
	  },
	  {
		"Goal": {
		  "Name": "/cred/createuserkeys",
		  "Desc": "Pending keys",
		  "Type": "Supertask",
		  "Creation": "2024-02-21T22:55:17.781866492Z",
		  "When": "2024-02-21T22:55:17.781250793Z"
		},
		"HighestStatus": "Pending",
		"LastUpdated": "2024-02-21T22:55:19.782265841Z",
		"Subtasks": [
		  {
			"Task": {
			  "ReconcilerManager": "cred",
			  "ReconcilerName": "createuserkeys",
			  "Desc": "Pending keys",
			  "LastStatus": "Pending",
			  "When": "2024-02-21T22:55:19.782265841Z",
			  "TaskKey": "/users/murphy",
			  "Messages": [
				{
				  "Level": "Warning",
				  "Message": "waiting for a reconciler to process this key..."
				}
			  ],
			  "CurrentStatus": "Pending"
			},
			"HighestStatus": "Pending",
			"LastUpdated": "2024-02-21T22:55:19.782265841Z"
		  }
		]
	  },
	  {
		"Goal": {
		  "Name": "/mergefs/userhome",
		  "Type": "Supertask",
		  "Creation": "2024-02-21T22:55:17.781866492Z",
		  "When": "2024-02-21T22:55:17.781250793Z"
		},
		"HighestStatus": "Success",
		"LastUpdated": "2024-02-21T22:55:17.849234032Z",
		"Subtasks": [
		  {
			"Task": {
			  "ReconcilerManager": "mergefs",
			  "ReconcilerName": "userhome",
			  "LastStatus": "Success",
			  "TaskVersion": "1",
			  "When": "2024-02-21T22:55:17.849234032Z",
			  "TaskKey": "/users/murphy",
			  "Messages": [
				{
				  "Level": "Info",
				  "Message": "task complete"
				}
			  ],
			  "CurrentStatus": "Success",
			  "TaskRevision": "2"
			},
			"HighestStatus": "Success",
			"LastUpdated": "2024-02-21T22:55:17.849234032Z"
		  }
		]
	  }
	]
  }
`

func TestOutput(t *testing.T) {
	tw := tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)
	a := assert.New(t)

	tf1 := new(rec.TaskForest)
	tf2 := new(rec.TaskForest)
	tf3 := new(rec.TaskForest)

	err := protojson.Unmarshal([]byte(forest1), tf1)
	a.Nil(err)
	WriteTaskForest(tf1, tw)

	err = protojson.Unmarshal([]byte(forest2), tf2)
	a.Nil(err)
	WriteTaskForest(tf2, tw)

	err = protojson.Unmarshal([]byte(forest3), tf3)
	a.Nil(err)
	WriteTaskForest(tf3, tw)
}
