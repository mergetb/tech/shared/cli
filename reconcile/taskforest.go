package reconcile

import (
	"fmt"
	"text/tabwriter"
	"time"

	rec "gitlab.com/mergetb/tech/reconcile"
	"gitlab.com/mergetb/tech/shared/cli"
)

func TaskForestToTable(tf *rec.TaskForest, parentpath ...string) *cli.Table {

	var sks []string
	var sks_levels []cli.TableItemStatus

	for _, sg := range tf.GetSubgoals() {
		sks = append(sks, sg.GetGoal().GetSelfKey())
		sks_levels = append(sks_levels, TaskStatusToCliLevel[sg.HighestStatus])
	}

	for _, st := range tf.GetSubtasks() {
		sks = append(sks, st.GetTask().GetFullName())
		sks_levels = append(sks_levels, TaskStatusToCliLevel[st.HighestStatus])
	}

	table := new(cli.Table)
	table.Title = fmt.Sprintf("%s: %s", tf.GetParentName(parentpath...), tf.GetGoal().GetDesc())

	switch tf.GetGoal().GetType() {
	case rec.TaskGoal_Supergoal:
		for _, sg := range tf.GetSubgoals() {

			level := TaskStatusToCliLevel[sg.HighestStatus]
			cur_row := []cli.TableItem{
				{
					Key:    "Subgoal",
					Value:  sg.GetGoal().GetName(),
					Status: level,
				},
				{
					Key:    "Desc",
					Value:  sg.GetGoal().GetDesc(),
					Status: level,
				},
				{
					Key:    "Status",
					Value:  sg.HighestStatus,
					Status: level,
				},
				{
					Key:    "Created",
					Value:  sg.GetGoal().GetCreation().AsTime().Format(time.UnixDate),
					Status: level,
				},
				{
					Key:    "Last Updated",
					Value:  sg.GetLastUpdated().AsTime().Format(time.UnixDate),
					Status: level,
				},
			}

			table.Rows = append(table.Rows, cur_row)
		}
	case rec.TaskGoal_Supertask:
		for _, st := range tf.GetSubtasks() {
			table.Rows = append(table.Rows, TaskTreeToTable(st).Rows...)
		}

	}

	return table
}

func WriteTaskForest(tf *rec.TaskForest, tw *tabwriter.Writer, parentpath ...string) error {

	if tf == nil {
		return nil
	}

	table := TaskForestToTable(tf, parentpath...)

	color := ReconcileColors[TaskStatusToCliLevel[tf.HighestStatus]]

	tw.Flush()
	table.TitleColor = color
	fmt.Fprint(tw, table.TitleToString())
	tw.Flush()

	table.ColumnsColor = cli.Blue
	fmt.Fprint(tw, table.ColumnsToString())
	fmt.Fprint(tw, table.RowsToString())
	tw.Flush()

	c_parent := append(parentpath, tf.Goal.GetName())
	for _, sg := range tf.GetSubgoals() {
		fmt.Fprintln(tw, "")
		WriteTaskForest(sg, tw, c_parent...)
	}

	tw.Flush()

	return nil

}
