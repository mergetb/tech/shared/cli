package reconcile

import (
	"fmt"
	"text/tabwriter"

	rec "gitlab.com/mergetb/tech/reconcile"
	"gitlab.com/mergetb/tech/shared/cli"
)

func TaskTreeToTable(tt *rec.TaskTree, parentpath ...string) *cli.Table {
	if tt == nil {
		return nil
	}

	table := TaskStatusToTable(tt.GetTask(), parentpath...)

	c_parent := append(parentpath, tt.Task.GetFullName())
	for _, stt := range tt.Subtasks {
		st := TaskTreeToTable(stt, c_parent...)

		table.Rows = append(table.Rows, st.Rows...)
	}

	return table

}

func WriteTaskTree(tt *rec.TaskTree, tw *tabwriter.Writer, h_color cli.Color, parentpath ...string) error {
	if tt == nil {
		return nil
	}

	table := TaskTreeToTable(tt, parentpath...)
	if table == nil {
		return fmt.Errorf("table is nil")
	}

	table.ColumnsColor = cli.Blue
	fmt.Fprintf(tw, table.ColumnsToString())
	fmt.Fprintf(tw, table.RowsToString())

	// don't need to print the subtasks,
	// as they're already in the table

	tw.Flush()
	return nil
}
