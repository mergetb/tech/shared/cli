package cli

import (
	"fmt"
	"strings"
	"unicode"

	"github.com/fatih/color"
)

type TableItemStatus int

const (
	ItemStatusNone TableItemStatus = iota
	ItemStatusOK
	ItemStatusWarn
	ItemStatusError
)

var (
	Blue     = color.New(color.FgHiBlue).SprintFunc()
	Black    = color.New(color.FgHiBlack).SprintFunc()
	White    = color.New(color.FgWhite).SprintFunc()
	HiRed    = color.New(color.FgHiRed).SprintFunc()
	LoRed    = color.New(color.FgRed).SprintFunc()
	HiGreen  = color.New(color.FgHiGreen).SprintFunc()
	LoGreen  = color.New(color.FgGreen).SprintFunc()
	Cyan     = color.New(color.FgCyan).SprintFunc()
	HiYellow = color.New(color.FgHiYellow).SprintFunc()
	LoYellow = color.New(color.FgYellow).SprintFunc()
	Magenta  = color.New(color.FgMagenta).SprintFunc()

	ItemStatusToColorDarkMode = map[TableItemStatus]func(...interface{}) string{
		ItemStatusNone:  White,
		ItemStatusOK:    HiGreen,
		ItemStatusWarn:  HiYellow,
		ItemStatusError: HiRed,
	}

	ItemStatusToColorLightMode = map[TableItemStatus]func(...interface{}) string{
		ItemStatusNone:  Black,
		ItemStatusOK:    LoGreen,
		ItemStatusWarn:  LoYellow,
		ItemStatusError: LoRed,
	}

	DefaultColor = Blue
)

type Color func(...interface{}) string

type Table struct {
	Title      string
	TitleColor Color

	Rows         [][]TableItem
	ColumnsColor Color
}

type TableItem struct {
	Key         interface{}
	Value       interface{}
	Status      TableItemStatus
	MultiStatus []TableItemStatus
}

func (t *Table) RowsToString() string {
	if t == nil {
		return ""
	}

	res := ""

	for _, r := range t.Rows {
		res += TableRow(r...)
	}

	return res
}

func TableRow(items ...TableItem) string {

	f_func := func(c rune) bool {
		return unicode.IsSpace(c) && c != ' '
	}

	template := strings.Repeat("%s\t", len(items))
	template = template[:len(template)-1] + "\n"

	maxLen := 0
	// Get the longest individual item
	for _, x := range items {
		l := len(strings.FieldsFunc(fmt.Sprint(x.Value), f_func))
		if l > maxLen {
			maxLen = l
		}
	}

	// make 2D array where:
	//   - the outer arrays are each row
	//   - the inner array are the items in each row
	split_data := make([][]interface{}, maxLen)
	for i := range split_data {
		split_data[i] = make([]interface{}, len(items))

		for j := range split_data[i] {
			split_data[i][j] = White("")
		}
	}

	for j, x := range items {
		for i, f := range strings.FieldsFunc(fmt.Sprint(x.Value), f_func) {
			status := x.Status
			if i < len(x.MultiStatus) {
				status = x.MultiStatus[i]
			}

			c, ok := ItemStatusToColorDarkMode[status]
			if !ok {
				c = White
			}

			split_data[i][j] = c(f)
		}
	}

	res := ""

	for _, d := range split_data {
		res += fmt.Sprintf(template, d...)
	}

	return res

}

func (t *Table) ColumnsToString() string {
	if t == nil {
		return ""
	}

	if len(t.Rows) == 0 {
		return ""
	}

	cols := make([]string, 0, len(t.Rows[0]))
	for _, i := range t.Rows[0] {
		cols = append(cols, fmt.Sprint(i.Key))
	}

	return TableColumns(t.ColumnsColor, cols...)
}

func TableColumns(color Color, columns ...string) string {

	if color == nil {
		color = DefaultColor
	}

	s := strings.Repeat("%s\t", len(columns))
	s = s[:len(s)-1] + "\n"
	// s += "\n"

	var cs []interface{}
	for _, x := range columns {
		cs = append(cs, color(x))
	}

	return fmt.Sprintf(s, cs...)

}

func (t *Table) TitleToString() string {
	if t == nil {
		return ""
	}

	return TableTitle(t.TitleColor, t.Title)
}

func (t *Table) ToString() string {
	if t == nil {
		return ""
	}

	res := ""

	res += strings.TrimRight(t.TitleToString(), "\n") + "\f"
	res += t.ColumnsToString()
	res += strings.TrimRight(t.RowsToString(), "\n") + "\f"

	return res
}

func TableTitle(color Color, title string) string {

	if color == nil {
		color = DefaultColor
	}

	title = strings.TrimSpace(title)
	spl := strings.Split(title, "\n")

	maxlen := 0
	for _, s := range spl {
		if len(s) > maxlen {
			maxlen = len(s)
		}
	}

	sep := strings.Repeat("=", maxlen) + "\n"

	res := fmt.Sprintf(title + "\n")
	res += sep

	return color(res)
}

func DisableColor() {
	color.NoColor = true
}

func EnableColor() {
	color.NoColor = false
}

func ColorEnabled() bool {
	return color.NoColor
}
